package com.example.zuul;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

public class PreFilter extends ZuulFilter {

	@Override
	public Object run() throws ZuulException {
		RequestContext ctx = RequestContext.getCurrentContext();
		HttpServletRequest request = ctx.getRequest();
		System.out.println("Request Method : " + request.getMethod() + " Request URL : " + request.getRequestURL().toString() + "- Parametros ");
		if(request.getHeader("nome").equals("Bruno"))
		{
			System.out.println("filtro aplicado");
		}
		
		
		
		
//		   Map<String, String> map = new HashMap<String, String>();
//		   
//	        Enumeration headerNames = request.getHeaderNames();
//	        while (headerNames.hasMoreElements()) {
//	            String key = (String) headerNames.nextElement();
//	            String value = request.getHeader(key);
//	            map.put(key, value);
//	        }
	        
//	        System.out.println(map.toString());
		return null;
	}

	@Override
	public boolean shouldFilter() {
		return true;
	}

	@Override
	public int filterOrder() {
		return 1;
	}

	@Override
	public String filterType() {
		return "pre";
	}
}
